package mbabilo;

import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class ParallelTest {

    public RemoteWebDriver driver;

    @BeforeTest
    @Parameters({"platform", "browserName", "remoteurl"})
    public void beforeTest(String platform, String browserName, String remoteurl) throws MalformedURLException {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setBrowserName("chrome");
        capabilities.setPlatform(Platform.extractFromSysProperty("platform"));
        capabilities.setVersion("ANY");
        driver = new RemoteWebDriver(new URL(remoteurl), capabilities);

    }
    @Test
    public void experitest(){
        String expectedTitle = "experitest.com";
        String actualTitle = "";
        driver.get("https://experitest.com/free-trial/");
        actualTitle = driver.getTitle();
        if (actualTitle.contentEquals(expectedTitle)){
            System.out.println("TEST PASSED!");
        } else {
            System.out.println("TEST FAILED");
        }
        driver.quit();
    }
}
